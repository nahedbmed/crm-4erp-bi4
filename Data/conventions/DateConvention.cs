﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.conventions
{
   public class DateConvention:Convention
    {
        public DateConvention()
        {
            Properties<DateTime>().Configure(p => p.HasColumnType("datetime2"));
        }
    }
}
