namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategorieProd",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nomCategorie = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produit",
                c => new
                    {
                        ProduitId = c.Int(nullable: false, identity: true),
                        nomProd = c.String(nullable: false, maxLength: 15),
                        unite = c.String(nullable: false),
                        qte = c.Int(nullable: false),
                        qteMin = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        description = c.String(nullable: false, maxLength: 50),
                        image = c.String(),
                        CategProdFK = c.Int(nullable: false),
                        MagasinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProduitId)
                .ForeignKey("dbo.Magasin", t => t.MagasinId, cascadeDelete: true)
                .ForeignKey("dbo.CategorieProd", t => t.CategProdFK, cascadeDelete: true)
                .Index(t => t.CategProdFK)
                .Index(t => t.MagasinId);
            
            CreateTable(
                "dbo.DevisProduit",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ProduitId = c.Int(),
                        DevisId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Devis", t => t.DevisId)
                .ForeignKey("dbo.Produit", t => t.ProduitId)
                .Index(t => t.ProduitId)
                .Index(t => t.DevisId);
            
            CreateTable(
                "dbo.Devis",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        DateCreation = c.DateTime(precision: 7, storeType: "datetime2"),
                        prix = c.Single(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Magasin",
                c => new
                    {
                        MagasinId = c.Int(nullable: false, identity: true),
                        nomMagasin = c.String(nullable: false),
                        adresse = c.String(),
                        ville = c.String(),
                    })
                .PrimaryKey(t => t.MagasinId);
            
            CreateTable(
                "dbo.Entrée",
                c => new
                    {
                        EntréeId = c.Int(nullable: false, identity: true),
                        Qte = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        DateEntrée = c.DateTime(precision: 7, storeType: "datetime2"),
                        produit_ProduitId = c.Int(),
                    })
                .PrimaryKey(t => t.EntréeId)
                .ForeignKey("dbo.Produit", t => t.produit_ProduitId)
                .Index(t => t.produit_ProduitId);
            
            CreateTable(
                "dbo.Facture",
                c => new
                    {
                        Id_Facture = c.Int(nullable: false, identity: true),
                        Date_bill = c.DateTime(precision: 7, storeType: "datetime2"),
                        typePaiement = c.Int(nullable: false),
                        Total = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id_Facture);
            
            CreateTable(
                "dbo.PanierProduits",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ProduitId = c.Int(),
                        PanierId = c.Int(),
                        qte = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Panier", t => t.PanierId)
                .ForeignKey("dbo.Produit", t => t.ProduitId)
                .Index(t => t.ProduitId)
                .Index(t => t.PanierId);
            
            CreateTable(
                "dbo.Panier",
                c => new
                    {
                        Id_Panier = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id_Panier)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        Email = c.String(maxLength: 256),
                        Password = c.String(nullable: false),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Sortie",
                c => new
                    {
                        SortieId = c.Int(nullable: false, identity: true),
                        Qte = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        DateSortie = c.DateTime(precision: 7, storeType: "datetime2"),
                        produit_ProduitId = c.Int(),
                    })
                .PrimaryKey(t => t.SortieId)
                .ForeignKey("dbo.Produit", t => t.produit_ProduitId)
                .Index(t => t.produit_ProduitId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sortie", "produit_ProduitId", "dbo.Produit");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.PanierProduits", "ProduitId", "dbo.Produit");
            DropForeignKey("dbo.Panier", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PanierProduits", "PanierId", "dbo.Panier");
            DropForeignKey("dbo.Entrée", "produit_ProduitId", "dbo.Produit");
            DropForeignKey("dbo.Produit", "CategProdFK", "dbo.CategorieProd");
            DropForeignKey("dbo.Produit", "MagasinId", "dbo.Magasin");
            DropForeignKey("dbo.DevisProduit", "ProduitId", "dbo.Produit");
            DropForeignKey("dbo.DevisProduit", "DevisId", "dbo.Devis");
            DropIndex("dbo.Sortie", new[] { "produit_ProduitId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Panier", new[] { "UserId" });
            DropIndex("dbo.PanierProduits", new[] { "PanierId" });
            DropIndex("dbo.PanierProduits", new[] { "ProduitId" });
            DropIndex("dbo.Entrée", new[] { "produit_ProduitId" });
            DropIndex("dbo.DevisProduit", new[] { "DevisId" });
            DropIndex("dbo.DevisProduit", new[] { "ProduitId" });
            DropIndex("dbo.Produit", new[] { "MagasinId" });
            DropIndex("dbo.Produit", new[] { "CategProdFK" });
            DropTable("dbo.Sortie");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Panier");
            DropTable("dbo.PanierProduits");
            DropTable("dbo.Facture");
            DropTable("dbo.Entrée");
            DropTable("dbo.Magasin");
            DropTable("dbo.Devis");
            DropTable("dbo.DevisProduit");
            DropTable("dbo.Produit");
            DropTable("dbo.CategorieProd");
        }
    }
}
