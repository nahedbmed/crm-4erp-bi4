﻿using System;
using Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using crm.Domain.Entity;
using crm.Data.conventions;
using crm.Data.configurations;

namespace Data
{
    public class PiDevContext : IdentityDbContext<User, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        
        public DbSet<Produit> produits { get; set; }
        public DbSet<CategorieProd> categorieProds { get; set; }
        public DbSet<Entrée> entrées { get; set; }
        public DbSet<Sortie> sorties { get; set; }
        public DbSet<Magasin> magasins { get; set; }
        public DbSet<Devis> Deviss { get; set; }
        public DbSet<Facture> Factures { get; set; }
        public DbSet<Panier> Paniers { get; set; }
        public DbSet<DevisProduit> DevisProduits { get; set; }
        public DbSet<PanierProduit> PanierProduits { get; set; }


        public PiDevContext() : base("name=DefaultConnection")
        {

        }

        public static PiDevContext Create()
        {
            return new PiDevContext();
        }

        static PiDevContext()
        {
            Database.SetInitializer<PiDevContext>(null);
        }

        //public DbSet<Client> Clients { get; set; }
        //public DbSet<Ressource> Ressources { get; set; }
       //public DbSet<Applicant> Applicants { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Add(new DateConvention());
            modelBuilder.Configurations.Add(new CategConfig());
            modelBuilder.Configurations.Add(new MagasinConfig());
        }

        
    }

    public class PiDevContextPerso : DropCreateDatabaseAlways<PiDevContext>
    {
        protected override void Seed(PiDevContext context)
        {


            context.SaveChanges();
        }
    }
}
