﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.configurations
{
    class MagasinConfig: EntityTypeConfiguration<Magasin>
    {


        public MagasinConfig()
        {
            HasMany(e => e.produits).WithRequired(t => t.magasin).HasForeignKey(e => e.MagasinId);

        }

    }
}
