﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.configurations
{
    class CategConfig : EntityTypeConfiguration<CategorieProd>
    {

        public CategConfig()
        {
            HasMany(e => e.produits).WithRequired(t => t.categorie).HasForeignKey(e => e.CategProdFK);

        }

    }
}
