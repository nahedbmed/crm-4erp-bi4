﻿using crm.Data.infrastructure;
using crm.ServicesPattern;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSpecifiques
{
    public class PanierProduitService : Service<PanierProduit>, IPanierProduitService
    {


        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork Uok = new UnitOfWork(Factory);
        public PanierProduitService() : base(Uok)
        {

        }



    }

}
