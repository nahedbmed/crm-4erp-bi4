﻿using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class DevisController : Controller
    {

        IDevisService service = new DevisService();
        IProduitService Service = new ProduitService();

        // GET: devis tous les devis pour l admin
        public ActionResult Index()
        {
            var devis = Service.GetAll();
            return View(devis);
        }

        // GET: Produit/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult AddDevis()
        {
            ProduitService ps = new ProduitService();
            AddDevisModel afm = new AddDevisModel();
            afm.produits = new List<Produit>();
            return View(afm);
        }
        public ActionResult AddDevis2(AddDevisModel m)
        {
           DevisService ans = new DevisService();
            Devis d = new Devis();
            d.DateCreation = new DateTime();
            d.Description = m.Description;
            d.prix = 10;
            ans.Add(d);
            ans.Commit();
            return RedirectToAction("Index", "Home");
        }



        // GET: Produit/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Produit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produit/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Produit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




    }
}