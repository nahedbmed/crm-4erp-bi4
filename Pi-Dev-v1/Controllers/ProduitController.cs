﻿using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Service;
using Domain.Entity;
using Microsoft.AspNet.Identity;
using Pi_Dev_v1.Models;
using ServiceSpecifiques;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class ProduitController : Controller
    {

        IProduitService Service = new ProduitService();

        // GET: Produit
        public ActionResult Index()
        {
            var produits = Service.GetAll();
            return View(produits);
        }

        // GET: Produit/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        public ActionResult AjouterPanier(int id)
        {
            IProduitService ps = new ProduitService();
            Produit p=ps.GetById(id);
            PanierProduit pp = new PanierProduit();
            pp.produit = p;
            pp.ProduitId = id;
            int userId=User.Identity.GetUserId<int>();
            IPanierService panierS = new PanierService();
            Panier panier = null;
            int i = panierS.GetAll().Where(e => e.UserId == userId).Count();
            if (i > 0)
            {
                panier = panierS.GetAll().Where(e => e.UserId == userId).First();
            }
            if (panier != null)
            {
                pp.panier = panier;
                pp.PanierId = panier.Id_Panier;
                pp.qte = 1;
            }
            else
            {
                panier = new Panier();
                panier.UserId = userId;
                panierS.Add(panier);
                panierS.Commit();
                pp.panier = panier;
                pp.PanierId = panier.Id_Panier;
                pp.qte = 1;
            }
            PanierProduitService pps = new PanierProduitService();
            pps.Add(pp);
            pps.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult MonPanier()
        {
            PanierModel pm = new PanierModel();
            IPanierService panierS = new PanierService();
            
            PanierProduitService pps = new PanierProduitService();
            pm.panierProduits=pps.GetAll().Where(e => e.PanierId == panierS.GetAll()
            .Where(ee => ee.UserId == User.Identity.GetUserId<int>()).First().Id_Panier).ToList();
            return View(pm);
        }
            

        // GET: Produit/Create
            public ActionResult Create()
        {
            ICategProduitService c = new CategProduitService();
            IMagasinService m = new MagasinService();
           // ProduitModel p = new ProduitModel();

           // p.magasins = m.GetMany().Select(e => new SelectListItem { Text = e.nomMagasin, Value = e.MagasinId.ToString() });
           // p.categories = c.GetMany().Select(e => new SelectListItem { Text = e.nomCategorie, Value = e.CategorieProdid.ToString() });

            var categorieProds = c.GetAll();
            var magasins = m.GetAll();

            ViewBag.CategList = new SelectList(categorieProds, "CategorieProdid", "nomCategorie");
            ViewBag.magasinList = new SelectList(magasins, "MagasinId", "nomMagasin");




            return View();
        }

        // POST: Produit/Create
        [HttpPost]
        public ActionResult Create(Produit p, HttpPostedFileBase Image)
        {



              p.image = Image.FileName;
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
             Image.SaveAs(path);



            //Produit p1 = new Produit()
            //{
            //    nomProd = "iphone11",
            //    prix = 2500,
            //    qte = 50,
            //    qteMin = 5,
            //    unite = "piece",
            //    image = "logo.jpg",
            //    description = "'iPhone 11 est également équipé du nouveau processeur Apple A13 Bionic et utilise un capteur Face ID amélioré. Il est proposé en plusieurs couleurs originales.",
            //    CategProdFK = 1,
            //    MagasinId = 1
            //};
            
            
          
                Service.Add(p);
                Service.Commit();
                
            


            
            return RedirectToAction("Index");
            }
            
        

        // GET: Produit/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Produit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produit/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Produit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
