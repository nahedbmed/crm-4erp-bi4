﻿using crm.Domain.Entity;
using crm.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class MagasinBackController : Controller
    {

        IMagasinService service = new MagasinService();


        // GET: MagasinBack
        public ActionResult Index()
        {
            return View(service.GetAll());
        }

        // GET: MagasinBack/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //GET: MagasinBack/Create
        public ActionResult Create()
        {
            

            return View();
        }

        // POST: MagasinBack/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Magasin m)
        {
            try
            {
              
                service.Add(m);
                service.Commit();
             
                return View("Index");
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: MagasinBack/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MagasinBack/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MagasinBack/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MagasinBack/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Magasin c1 = service.GetById(id);
                service.Delete(c1);
                service.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
