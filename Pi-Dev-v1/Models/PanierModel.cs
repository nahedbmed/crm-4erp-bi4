﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pi_Dev_v1.Models
{
    public class PanierModel
    {
        public List<PanierProduit> panierProduits { get; set; }
    }
}