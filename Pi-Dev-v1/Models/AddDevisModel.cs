﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crm.ProjetWeb.Models
{
    public class AddDevisModel 
    {
        public DateTime DateCreation { get; set; }
        public float prix { get; set; }
        public String Description { get; set; }
        public List<Produit> produits { get; set; }

    }
}