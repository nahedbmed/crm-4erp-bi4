﻿using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pi_Dev_v1.Models;
using Data;
using Domain.Entity;

[assembly: OwinStartupAttribute(typeof(Pi_Dev_v1.Startup))]
namespace Pi_Dev_v1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            AddUsersAndRoles();
        }


        private void AddUsersAndRoles()
        {

            PiDevContext context = new PiDevContext();
            var roleManager = new RoleManager<CustomRole, int>(new RoleStore<CustomRole, int, CustomUserRole>(context));
            var UserManager = new UserManager<User, int>(new UserStore<User, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>(context));

            // In Startup iam creating first Admin Role and creating a default Admin User    
            var role = new CustomRole();
            if (!roleManager.RoleExists("Client"))
            {


                role.Name = "Client";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("SuperAdmin"))
            {


                role.Name = "SuperAdmin";
                roleManager.Create(role);
            }
            if (UserManager.FindByName("SuperAdmin") == null)
            {
                var user = new User
                {
                    UserName = "admin@yahoo.com",

                    Email = "admin@yahoo.com",
                    Password = "Administration1234$"

                };

                var chkUser = UserManager.Create(user);
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "SuperAdmin")
                    ;
                }




            }

        }
    }
}
