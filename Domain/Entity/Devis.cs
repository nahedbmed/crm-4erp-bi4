﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("Devis")]
    public class Devis
    {


        [Key]
        [Column("id")]
        public int Devisid { get; set; }
        [DataType("datetime2")]
        public DateTime? DateCreation { get; set; }
        
       
        public float prix { get; set; }
        public String Description { get; set; }

        public ICollection<DevisProduit> devisProduits { get; set; }


        public Devis() { }


    }
    }
