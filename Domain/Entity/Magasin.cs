﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("Magasin")]
   public class Magasin
    {

        [Key]
        public int MagasinId { get; set; }
        [Required]
        [Display(Name = "Nom boutique")]
        public string nomMagasin { get; set; }

        public string adresse { get; set; }
        public string ville { get; set; }
        public ICollection<Produit> produits { get; set; }

        public Magasin() { }
    }
}
