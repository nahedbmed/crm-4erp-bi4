﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("DevisProduit")]
    public class DevisProduit
    {
        [Key]
        [Column("id")]
        public int DevisProduitid { get; set; }

        public int? ProduitId { get; set; }
        [ForeignKey("ProduitId")]
        public virtual Produit produit { get; set; }
        public int? DevisId { get; set; }
        [ForeignKey("DevisId")]
        public virtual Devis devis { get; set; }
    }
}
