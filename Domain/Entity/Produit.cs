﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("Produit")]
    public class Produit
    {

        [Key]
        public int ProduitId { get; set; }
        [Required, MaxLength(15)]
        public string nomProd { get; set; }
        [Required]
        public string unite { get; set; }
        [Required]

        public int qte { get; set; }
        [Required]
        public int qteMin { get; set; }
        [Required]
        public int prix { get; set; }


        [Required(ErrorMessage = " champs est obligatoire"), MaxLength(50)]
        [Display(Name = "Description du produit")]

        public string description { get; set; }



        public string image { get; set; }








        public int CategProdFK { get; set; }
        [ForeignKey("CategProdFK")]
        public CategorieProd categorie { get; set; }


        public int MagasinId { get; set; }
        [ForeignKey("MagasinFK")]
        public Magasin magasin { get; set; }



        
        public ICollection<DevisProduit> devisProduits { get; set; }


    }
}
