﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("Panier")]
    public class Panier
    {
        [Key]
        public int Id_Panier { get; set; }
        /*
        public int ClientId { get; set; }
        
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }
        */

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User user{ get; set; }

        public int? quantite;
        public ICollection<PanierProduit> panierProduits { get; set; }

    }
}
