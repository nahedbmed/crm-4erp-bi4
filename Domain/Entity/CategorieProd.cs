﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{



    [Table("CategorieProd")]
    public class CategorieProd
    {
        [Key]  [Column("id")]
        public int CategorieProdid { get; set; }

        [Required]
        [Display(Name = "Nom du catégorie")]

        public string nomCategorie { get; set; }
        public ICollection<Produit> produits { get; set; }

        public CategorieProd() { }


    }
}
