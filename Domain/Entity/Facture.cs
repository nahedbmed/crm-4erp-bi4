﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    public enum TypePaiement
    {
        Espece,
        En_ligne
    }
    [Table("Facture")]
    public class Facture
    {
        [Key]
        public int Id_Facture { get; set; }
        [DataType("datetime2")]
        public DateTime? Date_bill { get; set; }

       public TypePaiement typePaiement { get; set; }

        public double Total { get; set; }

        /*
              public int ClientId { get; set; }
              [ForeignKey("ClientId")]
              public virtual Client Client { get; set; }

              */
    }
}
