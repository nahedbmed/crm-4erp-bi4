﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entity
{
    public class PanierProduit
    {
        [Key]
        [Column("id")]
        public int PanierProduitid { get; set; }

        public int? ProduitId { get; set; }
        [ForeignKey("ProduitId")]
        public virtual Produit produit { get; set; }
        public int? PanierId { get; set; }
        [ForeignKey("PanierId")]
        public virtual Panier panier { get; set; }

        public int qte { get; set; }

    }
}
