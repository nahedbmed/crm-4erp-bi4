﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("Sortie")]
    public class Sortie
    {
        [Key]
        public int SortieId { get; set; }
        public Produit produit { get; set; }
        public int Qte { get; set; }
        public int prix { get; set; }
        [DataType("datetime2")]
        public DateTime? DateSortie { get; set; }

        public Sortie() { }




    }
}
